#!/usr/bin/python
import os
import sys
import multiprocessing
import collections
# specify where the ns was installed when move to ccis
# for develop purpose, we just use ns
which_ns = 'ns'
TCP = [
    'Tahoe',
    'Reno',
    'NewReno',
    'Vegas'
]


class Tcp:
    def __init__(self):
        self.start = 0.0  # start time of the packet send
        self.start_flag = False
        self.end = 0.0    # end time of the last packet received
        self.total = 0.0  # total packed received
        self.throughput = 0.0
        self.send = 0.0  # total packet sent
        self.drop = 0.0  # total packet dropped
        self.latency = collections.defaultdict(list)  # packet id -> [start_time, receive_time]
        self.total_trans_time = 0.0
        self.total_received = 0.0


class Packet:
    def __init__(self, content):
        # all the data will be in string format, convert format when needed
        content = content.split(' ')
        self.event = content[0]
        self.time = content[1]
        self.from_node = content[2]
        self.to_node = content[3]
        self.pkt_type = content[4]
        self.pkt_size = content[5]
        self.fid = content[7]
        self.src_addr = content[8]
        self.dst_addr = content[9]
        self.seq_num = content[10]
        self.pkt_id = int(content[11])


def analyze_throughput1(contents):
    # return the throughput
    tcp = Tcp()
    for content in contents:
        if ' cbr ' in content:
            continue
        packet = Packet(content)
        if not tcp.start_flag and packet.from_node == '0' and packet.event == '+':
            tcp.start_flag = True
            tcp.start = float(packet.time)
        # we only calculate the packet arrived at N4
        if packet.to_node == '3' and packet.event == 'r':
            tcp.end = float(packet.time)
            tcp.total += float(packet.pkt_size)
    #  convert to mbs
    if tcp.total != 0.0:
        return (8 * tcp.total / 1024 / 1024) / (tcp.end - tcp.start)
    else:
        return 0


def analyze_latency1(contents):
    tcp = Tcp()
    for content in contents:
        if ' cbr ' in content:
            continue
        packet = Packet(content)
        if packet.from_node == '0' and packet.event == '+':
            tcp.latency[packet.seq_num].append(float(packet.time))
        if packet.to_node == '0' and packet.event == 'r':
            tcp.latency[packet.seq_num].append(float(packet.time))
            tcp.total_received += 1
    if tcp.total_received != 0.0:
        tcp.total_received = 0
        for item in tcp.latency:
            item = tcp.latency[item]
            if len(item) < 2:
                continue
            tcp.total_trans_time += (item[1] - item[0])
            tcp.total_received += 1
        return tcp.total_trans_time / tcp.total_received
    else:
        return 0.0


def analyze1(rate):
    f = open('experiment1.tr', 'r').readlines()
    throughput = analyze_throughput1(f)
    drop = analyze_pack_drop1(f)
    latency = analyze_latency1(f)
    return formatter(rate, throughput, drop, latency)


def formatter(rate, throughput, drop, latency):
    res = str(rate)
    while len(res) < 12:
        res += ' '
    res += str(throughput)
    while len(res) < 33:
        res += ' '
    res += str(drop)
    while len(res) < 51:
        res += ' '
    if latency == 0 or latency == 0.0:
        #  blocked
        res += 'Infinity'
    else:
        res += str(latency)
    return res + '\n'


def analyze_pack_drop1(contents):
    tcp = Tcp()
    for line in contents:
        if ' cbr ' in line:
            continue
        packet = Packet(line)
        if packet.from_node == '0' and packet.event == '+':
            tcp.send += 1
        if packet.fid == '1' and packet.event == 'd':
            tcp.drop += 1
    return tcp.drop * 100 / tcp.send


def do_experiment1():
    f = open('experiment1.result', 'w')
    for tcp in TCP:
        f.writelines(tcp + '\n')
        f.writelines('cbr(mbs)    throughput(mb/s)     packet-drop(%)    avg-latency(s)\n')
        for i in xrange(1, 20):
            command = which_ns + ' experiment1.tcl ' + tcp + ' ' + str(i)
            os.system(command)
            f.writelines(analyze1(i))
    f.close()
    pass


def analyze2(rate):
    f = open('experiment2.tr', 'r').readlines()
    throughput = analyze_throughput2(f)
    drop_rate = analyze_pack_drop2(f)
    latency = analyze_latency2(f)
    # print throughput, drop_rate, latency
    return [formatter(rate, throughput[0], drop_rate[0], latency[0]),
            formatter(rate, throughput[1], drop_rate[1], latency[1])]
    pass


def analyze_latency2(contents):
    tcp1 = Tcp()
    tcp2 = Tcp()
    for line in contents:
        if ' cbr ' in line:
            continue
        packet = Packet(line)
        if packet.from_node == '0' and packet.event == '+':
            tcp1.latency[packet.seq_num].append(float(packet.time))
        if packet.to_node == '3' and packet.event == 'r':
            tcp1.total_received += 1
            tcp1.latency[packet.seq_num].append(float(packet.time))
        if packet.from_node == '4' and packet.event == '+':
            tcp2.latency[packet.seq_num].append(float(packet.time))
        if packet.to_node == '5' and packet.event == 'r':
            tcp2.total_received += 1
            tcp2.latency[packet.seq_num].append(float(packet.time))
    late1 = 0.0
    late2 = 0.0
    if tcp1.total_received != 0.0:
        tcp1.total_received = 0
        for item in tcp1.latency:
            item = tcp1.latency[item]
            if len(item) < 2:
                continue
            tcp1.total_trans_time += (item[1] - item[0])
            tcp1.total_received += 1
        late1 = tcp1.total_trans_time / tcp1.total_received
    if tcp2.total_received != 0.0:
        tcp2.total_received = 0
        for item in tcp2.latency:
            if len(tcp2.latency[item]) < 2:
                continue
            tcp2.total_trans_time += (tcp2.latency[item][1] - tcp2.latency[item][0])
            tcp2.total_received += 1
        late2 = tcp2.total_trans_time / tcp2.total_received
    return [late1, late2]


def analyze_pack_drop2(contents):
    tcp1 = Tcp()
    tcp2 = Tcp()
    for line in contents:
        if ' cbr ' in line:
            continue
        packet = Packet(line)
        if packet.from_node == '0' and packet.event == '+':
            tcp1.send += 1
        if packet.fid == '1' and packet.event == 'd':
            tcp1.drop += 1
        if packet.from_node == '4' and packet.event == '+':
            tcp2.send += 1
        if packet.fid == '2' and packet.event == 'd':
            tcp2.drop += 1
    return [tcp1.drop * 100 / tcp1.send, tcp2.drop * 100 / tcp2.send]


def analyze_throughput2(contents):
    tcp1 = Tcp()
    tcp2 = Tcp()
    for line in contents:
        if ' cbr ' in line:
            continue
        packet = Packet(line)
        if not tcp1.start_flag and packet.from_node == '0' and packet.event == '+':
            tcp1.start_flag = True
            tcp1.start = float(packet.time)
        if not tcp2.start_flag and packet.from_node == '4' and packet.event == '+':
            tcp2.start_flag = True
            tcp2.start = float(packet.time)
        if packet.to_node == '3' and packet.event == 'r':
            tcp1.end = float(packet.time)
            tcp1.total += float(packet.pkt_size)
        if packet.to_node == '5' and packet.event == 'r':
            tcp2.end = float(packet.time)
            tcp2.total += float(packet.pkt_size)
    if tcp1.total != 0.0:
        tcp1.throughput = (8 * tcp1.total / 1024 / 1024) / (tcp1.end - tcp1.start)
    if tcp2.total != 0.0:
        tcp2.throughput = (8 * tcp2.total / 1024 / 1024) / (tcp2.end - tcp2.start)
    return [tcp1.throughput, tcp2.throughput]


def do_experiment2():
    f = open('experiment2.result', 'w')
    combination = [
        ['Reno', 'Reno'],
        ['NewReno', 'Reno'],
        ['Vegas', 'Vegas'],
        ['NewReno', 'Vegas']
    ]
    for tcp in combination:
        f.writelines('TCP: ' + tcp[0] + '/' + tcp[1] + '\n')
        f.writelines('cbr(mbs)    throughput(mb/s)     packet-drop(%)    avg-latency(s)\n')
        for i in xrange(1, 20):
            command = which_ns + ' experiment2.tcl ' + tcp[0] + ' ' + tcp[1] + ' ' + str(i)
            os.system(command)
            res = analyze2(i)
            f.writelines(res[0])
            f.writelines(res[1])
    f.close()
    pass


def main():
    # p_pool = multiprocessing.Pool()
    # p_pool.apply_async(do_experiment1())
    # p_pool.apply_async(do_experiment2())
    # do_experiment1()
    do_experiment2()
    # do_experiment3()
    # p_pool.close()
    # p_pool.join()


if __name__ == '__main__':
    main()
