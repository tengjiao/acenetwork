set term png
set output "latency3.png"
set title "Experienmnet 3 Latency"
set xlabel "Time[s]"
set ylabel "Latency[ms]"
set grid
plot 'latency3.dat' using 1:2 with lp pt 2 lw 1 linecolor rgb "red" title 'Reno-DropTail-TCP',\
 'latency3.dat' using 1:3 with lp pt 2 lw 1 linecolor rgb "#C73852" title 'Reno-DropTail-CBR', \
 'latency3.dat' using 1:4 with lp pt 2 lw 1 linecolor rgb "#EB990C" title 'Reno-RED-TCP', \
 'latency3.dat' using 1:5 with lp pt 2 lw 1 linecolor rgb "#826127" title 'Reno-RED-CBR', \
 'latency3.dat' using 1:6 with lp pt 2 lw 1 linecolor rgb "blue" title 'SACK-DropTail-TCP', \
 'latency3.dat' using 1:7 with lp pt 2 lw 1 linecolor rgb "#1361A1" title 'SACK-DropTail-CBR', \
 'latency3.dat' using 1:8 with lp pt 2 lw 1 linecolor rgb "#19AB09" title 'SACK-RED-TCP', \
 'latency3.dat' using 1:9 with lp pt 2 lw 1 linecolor rgb "#0C5704" title 'SACK-RED-CBR'

