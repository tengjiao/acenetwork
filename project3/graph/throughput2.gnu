set term png
set output "throughput2.png"
set title "Experienmnet 2 Vegas/Vegas Throughtput"
set xlabel "CBR[Mbps]"
set ylabel "Throughput[Mbps]"
set grid
plot 'throughput2.dat' using 1:2 with lp pt 2 lw 1 linecolor rgb "red" title 'Vegas[1-4]',\
 'throughput2.dat' using 1:3 with lp pt 8 lw 1 lt 1 linecolor rgb "blue" title 'Vegas[5-6]'
