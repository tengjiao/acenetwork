set term png
set output "latency2.png"
set title "Experienmnet 2 NewReno/Vegas Latency"
set xlabel "CBR[Mbps]"
set ylabel "Latency[s]"
set grid
set key left
plot 'latency2.dat' using 1:2 with lp pt 2 lw 1 linecolor rgb "red" title 'NewReno[1-4]',\
 'latency2.dat' using 1:3 with lp pt 8 lw 1 lt 1 linecolor rgb "blue" title 'Vegas[5-6]'
