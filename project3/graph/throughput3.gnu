set term png
set output "throughput3.png"
set title "Experienmnet 3 Throughtput"
set xlabel "Time[s]"
set ylabel "Throughput[Mbps]"
set grid
set key left
plot 'throuput3.dat' using 1:2 with lp pt 2 lw 1 linecolor rgb "red" title 'Reno-DropTail-TCP',\
 'throuput3.dat' using 1:3 with lp pt 2 lw 1 linecolor rgb "red" title 'Reno-DropTail-CBR', \
 'throuput3.dat' using 1:4 with lp pt 2 lw 1 linecolor rgb "#EB990C" title 'Reno-RED-TCP', \
 'throuput3.dat' using 1:5 with lp pt 2 lw 1 linecolor rgb "#EB990C" title 'Reno-RED-CBR', \
 'throuput3.dat' using 1:6 with lp pt 2 lw 1 linecolor rgb "blue" title 'SACK-DropTail-TCP', \
 'throuput3.dat' using 1:7 with lp pt 2 lw 1 linecolor rgb "blue" title 'SACK-DropTail-CBR', \
 'throuput3.dat' using 1:8 with lp pt 2 lw 1 linecolor rgb "#19AB09" title 'SACK-RED-TCP', \
 'throuput3.dat' using 1:9 with lp pt 2 lw 1 linecolor rgb "#19AB09" title 'SACK-RED-CBR'
