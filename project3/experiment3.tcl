#Create a simulator object
set ns [new Simulator]

# get argument from command line
set tcp [lindex $argv 0]
set queue_algo [lindex $argv 1]

#Open the Trace file
set tf [open experiment3.tr w]
$ns trace-all $tf

Agent/TCP set window_ 500

#Define a 'finish' procedure
proc finish {} {
        global ns tf
        $ns flush-trace
        #Close the Trace file
        close $tf
        #Execute NAM on the trace file
        exit 0
}

#Create six nodes
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]

#Create links between the nodes
$ns duplex-link $n1 $n2 10Mb 10ms $queue_algo
$ns duplex-link $n2 $n3 10Mb 10ms $queue_algo
$ns duplex-link $n3 $n4 10Mb 10ms $queue_algo
$ns duplex-link $n3 $n6 10Mb 10ms $queue_algo
$ns duplex-link $n5 $n2 10Mb 10ms $queue_algo

# $ns queue-limit $n1 $n2 10
# $ns queue-limit $n2 $n3 10
# $ns queue-limit $n3 $n4 10
# $ns queue-limit $n6 $n3 10
# $ns queue-limit $n5 $n2 10

#Setup a TCP connection
if {$tcp eq "SACK"} {
  set sink [new Agent/TCPSink/Sack1]
  set tcp [new Agent/TCP/Sack1]
} else {
  set tcp [new Agent/TCP/Reno]
  set sink [new Agent/TCPSink]
}

$ns attach-agent $n1 $tcp
$ns attach-agent $n4 $sink
$ns connect $tcp $sink
$tcp set fid_ 1


#Setup a FTP over TCP connection
set ftp [new Application/FTP]
$ftp attach-agent $tcp
$ftp set type_ FTP

#Setup a UDP connection
set udp [new Agent/UDP]
$ns attach-agent $n5 $udp
set null [new Agent/Null]
$ns attach-agent $n6 $null
$ns connect $udp $null
$udp set fid_ 2

#Setup a CBR over UDP connection
set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type_ CBR
$cbr set packet_size_ 1000
$cbr set rate_ 8mb
$cbr set random_ false

#Schedule events for the CBR and FTP agents
$ns at 0.0 "$ftp start"
$ns at 4.0 "$cbr start"
$ns at 20.0 "$ftp stop"
$ns at 20.0 "$cbr stop"

#Call the finish procedure after 10.1 seconds of simulation time
$ns at 20.6 "finish"

#Run the simulation
$ns run
