#!/usr/bin/python
import os
import sys
import multiprocessing
import collections
import math

which_ns = 'ns'

class Tcp:
    def __init__(self, _id):
        self.id = _id
        self.start = 0.0  # start time of the packet send
        self.start_flag = False
        self.end = 0.0    # end time of the last packet received
        self.total = 0.0  # total packed received
        self.throughput = 0.0
        self.send = 0.0  # total packet sent
        self.drop = 0.0  # total packet dropped
        self.latency = collections.defaultdict(list)  # packet id -> [start_time, receive_time]
        self.total_trans_time = 0.0
        self.total_received = 0.0
        self.late = 0.0


class Packet:
    def __init__(self, content):
        # all the data will be in string format, convert format when needed
        content = content.split()
        self.event = content[0]
        self.time = content[1]
        self.from_node = content[2]
        self.to_node = content[3]
        self.pkt_type = content[4]
        self.pkt_size = content[5]
        self.fid = content[7]
        self.src_addr = content[8]
        self.dst_addr = content[9]
        self.seq_num = int(content[10])
        self.pkt_id = int(content[11])


def throughput(contents):
    tcp = {}
    cbr = {}
    for x in xrange(3, 20):
        tcp[x] = Tcp(x)
        cbr[x] = Tcp(x)
    for line in contents:
        packet = Packet(line)
        if float(packet.time) < 3:
            continue
        if float(packet.time) >= 20:
            break
        time = int(math.floor(float(packet.time)))
        if not tcp[time].start_flag and packet.from_node == '0' and packet.event == '+':
            tcp[time].start_flag = True
            tcp[time].start = float(packet.time)
        if not cbr[time].start_flag and packet.from_node == '4' and packet.event == '+':
            cbr[time].start_flag = True
            cbr[time].start = float(packet.time)
        if packet.to_node == '3' and packet.event == 'r':
            tcp[time].end = float(packet.time)
            tcp[time].total += float(packet.pkt_size)
        if packet.to_node == '5' and packet.event == 'r':
            cbr[time].end = float(packet.time)
            cbr[time].total += float(packet.pkt_size)
    for x in xrange(3, 20):
        if tcp[x].total != 0.0:
            tcp[x].throughput = (8 * tcp[x].total / 1024/ 1024) / (tcp[x].end - tcp[x].start)
        if cbr[x].total != 0.0:
            cbr[x].throughput = (8 * cbr[x].total / 1024 / 1024) / (cbr[x].end - cbr[x].start)
    result = []
    for x in xrange(3,20):
        res = [x, tcp[x].throughput, cbr[x].throughput]
        result.append(res)
    return result

def droprate(contents):
    tcp = {}
    cbr = {}
    for x in xrange(4, 20):
        tcp[x] = Tcp(x)
        cbr[x] = Tcp(x)
    for line in contents:
        packet = Packet(line)
        if float(packet.time) < 4:
            continue
        if float(packet.time) >= 20:
            break
        time = int(math.floor(float(packet.time)))
        if packet.from_node == '0' and packet.event == '+':
            tcp[time].send += 1
        if packet.from_node == '4' and packet.event == '+':
            cbr[time].send += 1
        if packet.event == 'd' and packet.fid == '1':
            tcp[time].drop += 1
        if packet.event == 'd' and packet.fid == '2':
            cbr[time].drop += 1
    result = []
    for x in xrange(4, 20):
        res = [x, tcp[x].drop * 100 / tcp[x].send, cbr[x].drop * 100 / cbr[x].send]
        result.append(res)
    return result

def latency(contents):
    tcp = {}
    cbr = {}
    for x in xrange(4, 20):
        tcp[x] = Tcp(x)
        cbr[x] = Tcp(x)
    for line in contents:
        packet = Packet(line)
        if float(packet.time) < 4:
            continue
        if float(packet.time) >= 20:
            break
        time = int(math.floor(float(packet.time)))
        if packet.from_node == '0' and packet.event == '+':
            tcp[time].latency[packet.seq_num].append(float(packet.time))
        if packet.from_node == '4' and packet.event == '+':
            cbr[time].latency[packet.seq_num].append(float(packet.time))
        if packet.to_node == '0' and packet.event == 'r':
            tcp[time].latency[packet.seq_num].append(float(packet.time))
            tcp[time].total_received += 1
        if packet.to_node == '5' and packet.event == 'r':
            cbr[time].latency[packet.seq_num].append(float(packet.time))
            cbr[time].total_received += 1
    for x in xrange(4, 20):
        temp_all = 0.0
        for item in tcp[x].latency:
            if len(tcp[x].latency[item]) < 2:
                continue
            temp_all += (tcp[x].latency[item][1] - tcp[x].latency[item][0])
        if temp_all != 0.0:
            tcp[x].late = temp_all / tcp[x].total_received
        temp_all = 0.0
        for item in cbr[x].latency:
            if len(cbr[x].latency[item]) < 2:
                continue
            temp_all += (cbr[x].latency[item][1] - cbr[x].latency[item][0])
        if temp_all != 0.0:
            cbr[x].late = temp_all / cbr[x].total_received
    result = []
    for x in xrange(4, 20):
        res = [x, tcp[x].late, cbr[x].late]
        result.append(res)
    return result
    pass


def do_experiment3():
    f = open('experiment3.result', 'w')
    q_algo = ['DropTail', 'RED']
    tcp_vari = ['Reno', 'SACK']
    for tcp in tcp_vari:
        for q in q_algo:
            f.writelines('TCP: ' + tcp + ' Queing: ' + q + ' throughput(mb/s)\n')
            command = which_ns + ' experiment3.tcl ' + tcp + ' ' + q
            os.system(command)
            contents = open('experiment3.tr','r').readlines()
            for line in throughput(contents):
                f.writelines(str(line[0]) + ' ' + str(line[1]) + ' ' + str(line[2]) + '\n')
            f.writelines(' drop rate \n')
            for line in droprate(contents):
                f.writelines(str(line[0]) + ' ' + str(line[1]) + ' ' + str(line[2]) + '\n')
            f.writelines(' latency \n')
            for line in latency(contents):
                f.writelines(str(line[0]) + ' ' + str(line[1]) + ' ' + str(line[2]) + '\n')
    f.close()

def main():
    do_experiment3()



def test():
    # os.system(which_ns + ' experiment3.tcl fuck DropTail')
    tcp = {}
    f = open('experiment3.tr','r').readlines()
    for x in xrange(0, 4):
        tcp[x] = Tcp(x)
    for line in f:
        packet = Packet(line)
        if float(packet.time) >= 4:
            break
        time = int(math.floor(float(packet.time)))
        # print packet.time, time
        if not tcp[time].start_flag and packet.event == '+':
            tcp[time].start_flag = True
            tcp[time].start = float(packet.time)
        if packet.to_node == '3' and packet.event == 'r':
            tcp[time].end = float(packet.time)
            tcp[time].total += float(packet.pkt_size)
    for x in xrange(0, 4):
        tcp[x].throughput = 8 * tcp[x].total / 1024 / 1024
        print x, tcp[x].throughput
    # for x in xrange(0, 4):
    #     print x, tcp[x]

if __name__ == '__main__':
    main()
    # test()
