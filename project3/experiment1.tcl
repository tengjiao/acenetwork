#Create a simulator object
set ns [new Simulator]

# get argument from command line
set tcp [lindex $argv 0]
set cbr_rate [lindex $argv 1]

#Open the Trace file
set tf [open experiment1.tr w]
$ns trace-all $tf

# Agent/TCP set packetSize_ 1000
# Agent/TCPSink set packetSize_ 40
# Agent/TCP set window_ 2000


#Define a 'finish' procedure
proc finish {} {
        global ns tf
        $ns flush-trace
        #Close the Trace file
        close $tf
        exit 0
}

#Create six nodes
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]

#Create links between the nodes
$ns duplex-link $n1 $n2 10Mb 10ms DropTail
$ns duplex-link $n2 $n3 10Mb 10ms DropTail
$ns duplex-link $n3 $n4 10Mb 10ms DropTail
$ns duplex-link $n3 $n6 10Mb 10ms DropTail
$ns duplex-link $n2 $n5 10Mb 10ms DropTail


#Setup a TCP connection
if {$tcp eq "Reno"} {
  set tcp [new Agent/TCP/Reno]
} elseif {$tcp eq "NewReno"} {
  set tcp [new Agent/TCP/Newreno]
} elseif {$tcp eq "Vegas"} {
  set tcp [new Agent/TCP/Vegas]
} else {
  # default option is Tahoe
  set tcp [new Agent/TCP]
}
$tcp set class_ 1
$ns attach-agent $n1 $tcp
set sink [new Agent/TCPSink]
$ns attach-agent $n4 $sink
$ns connect $tcp $sink
$tcp set fid_ 1

#Setup a FTP over TCP connection
set ftp [new Application/FTP]
$ftp attach-agent $tcp
$ftp set type_ FTP

#Setup a UDP connection
set udp [new Agent/UDP]
$ns attach-agent $n2 $udp
set null [new Agent/Null]
$ns attach-agent $n3 $null
$ns connect $udp $null

#Setup a CBR over UDP connection
set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type_ CBR
# packet size, use default?
$cbr set packet_size_ 1000
$cbr set rate_ ${cbr_rate}mb
$cbr set random_ false


#Schedule events for the CBR and FTP agents
$ns at 0.1 "$cbr start"
$ns at 1.0 "$ftp start"
$ns at 20.0 "$ftp stop"
$ns at 20.1 "$cbr stop"

#Call the finish procedure after 10.1 seconds of simulation time
$ns at 20.2 "finish"

#Run the simulation
$ns run
