#Create a simulator object
set ns [new Simulator]

# get argument from command line
set tcp_1 [lindex $argv 0]
set tcp_2 [lindex $argv 1]
set cbr_rate [lindex $argv 2]

Agent/TCP set packetSize_ 1000
Agent/TCPSink set packetSize_ 40
Agent/TCP set window_ 2000

#Open the Trace file
set tf [open experiment2.tr w]
$ns trace-all $tf


#Define a 'finish' procedure
proc finish {} {
        global ns tf
        $ns flush-trace
        #Close the Trace file
        close $tf
        #Execute NAM on the trace file
        exit 0
}

#Create six nodes
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]

#Create links between the nodes
$ns duplex-link $n1 $n2 10Mb 10ms DropTail
$ns duplex-link $n2 $n3 10Mb 10ms DropTail
$ns duplex-link $n3 $n4 10Mb 10ms DropTail
$ns duplex-link $n3 $n6 10Mb 10ms DropTail
$ns duplex-link $n2 $n5 10Mb 10ms DropTail


#Setup a TCP connection
if {$tcp_1 eq "Reno"} {
  set tcp1 [new Agent/TCP/Reno]
} elseif {$tcp_1 eq "NewReno"} {
  set tcp1 [new Agent/TCP/Newreno]
} elseif {$tcp_1 eq "Vegas"} {
  set tcp1 [new Agent/TCP/Vegas]
  $tcp1 set packetSize_ 1040
} else {
  # default option is Tahoe
  set tcp1 [new Agent/TCP]
}
# setup another TCP connection
if {$tcp_2 eq "Reno"} {
    set tcp2 [new Agent/TCP/Reno]
} elseif {$tcp_2 eq "NewReno"} {
    set tcp2 [new Agent/TCP/Newreno]
} elseif {$tcp_2 eq "Vegas"} {
    set tcp2 [new Agent/TCP/Vegas]
    $tcp2 set packetSize_ 1040
} else {
    set tcp2 [new Agent/TCP]
}
$tcp1 set class_ 2
$ns attach-agent $n1 $tcp1
set sink1 [new Agent/TCPSink]
$ns attach-agent $n4 $sink1
$ns connect $tcp1 $sink1
$tcp1 set fid_ 1

$tcp2 set class_ 2
$ns attach-agent $n5 $tcp2
set sink2 [new Agent/TCPSink]
$ns attach-agent $n6 $sink2
$ns connect $tcp2 $sink2
$tcp2 set fid_ 2

#Setup a FTP over TCP connection
set ftp1 [new Application/FTP]
$ftp1 attach-agent $tcp1
$ftp1 set type_ FTP

set ftp2 [new Application/FTP]
$ftp2 attach-agent $tcp2
$ftp2 set type_ FTP

#Setup a UDP connection
set udp [new Agent/UDP]
$ns attach-agent $n2 $udp
set null [new Agent/Null]
$ns attach-agent $n3 $null
$ns connect $udp $null

#Setup a CBR over UDP connection
set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type_ CBR
$cbr set packet_size_ 1000
$cbr set rate_ ${cbr_rate}mb
$cbr set random_ false


#Schedule events for the CBR and FTP agents
$ns at 0.0 "$cbr start"
$ns at 0.0 "$ftp1 start"
$ns at 0.0 "$ftp2 start"
$ns at 34.0 "$ftp1 stop"
$ns at 34.0 "$ftp2 stop"
$ns at 34.1 "$cbr stop"

#Call the finish procedure after 10.1 seconds of simulation time
$ns at 34.2 "finish"

#Run the simulation
$ns run
