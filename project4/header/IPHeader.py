import socket
import binascii
from struct import pack, unpack
from random import randint
import sys

# IP Header Reference (Thanks to Silver Moon)

#     0               1               2               3   
#     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |Version|  IHL  |Type of Service|          Total Length         |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |         Identification        |Flags|      Fragment Offset    |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |  Time to Live |    Protocol   |         Header Checksum       |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                       Source Address                          |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                    Destination Address                        |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                    Options                    |    Padding    |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# should be called IP header
class IPHeader:
  '''
  Constructor
  '''
  def __init__(self, sourceIP, descIP):
    # ip header fields
    # 1st line
    self.ver = 4
    self.ihl = 5
    self.tos = 0
    self.tot_len = 0 # kernel will fill the correct total length
    
    # 2nd line
    self.id = randint(0, 65535)
    self.flag_RF = 0 # bit 0: Reserved; must be zero.
    self.flag_DF = 1 # bit 1: Don't Fragment (DF)
    self.flag_MF = 0 # bit 2: More Fragments (MF)
    self.frag_off = 0
    
    # 3rd line
    self.ttl = 255
    self.proto = socket.IPPROTO_TCP
    self.check = 0
    
    # 4th, 5th line
    self.saddr = socket.inet_aton ( sourceIP )
    self.daddr = socket.inet_aton ( descIP )

  '''
  Pack the packet into struct of OS c code
  '''
  def pack(self):
    ihl_ver = (self.ver << 4) + self.ihl
    flag_frag_off = (self.flag_RF << 15) +\
                    (self.flag_DF << 14) +\
                    (self.flag_MF << 13) +\
                    self.frag_off
    
    # B: 1B, H:2B, 4s:4B
    # BBH -> 4B
    # HH  -> 4B
    # BBH -> 4B 
    # 4s  -> 4B, 4s  -> 4B 
    ip_header = pack('!BBHHHBBH4s4s' ,\
                      ihl_ver, self.tos, self.tot_len,\
                      self.id, flag_frag_off,\
                      self.ttl, self.proto, self.check,\
                      self.saddr,\
                      self.daddr)
    return ip_header

  '''
  Unpack the packet into object of this python code
  
  Return the data following ip header
  '''
  def unpack(self, raw_data):
    [ihl_ver, self.tos, self.tot_len] = unpack("!BBH", raw_data[:4])
    [self.id, flag_frag_off] = unpack("!HH", raw_data[4:8])
    [self.ttl, self.proto, self.check] = unpack("!BBH", raw_data[8:12])
    [sourceIP_b, descIP_b] = unpack("!4s4s", raw_data[12:20])

    self.ver = (ihl_ver & 0xF0) >> 4
    self.ihl = (ihl_ver & 0x0F)

    self.flag_RF = (flag_frag_off >> 15) & 0x0001
    self.flag_DF = (flag_frag_off >> 14) & 0x0001
    self.flag_MF = (flag_frag_off >> 13) & 0x0001
    self.frag_off = flag_frag_off & 0x1FFF

    self.saddr = socket.inet_ntoa ( sourceIP_b )
    self.daddr = socket.inet_ntoa ( descIP_b )

    data = raw_data[20: self.tot_len]
    
    # The check-sum 2 byte[10:12] need to be cleared.
    header = raw_data[:10] + pack('H', 0) + raw_data[12: 20]

    _check = self.__checksum(header)

    if _check != self.check:
      #print "IP Checksum does not match!"
      #print "Should be" 
      #print _check
      #print "Actual" 
      #print self.check
      return None
  
    return data


  '''
  checksum functions needed for calculation checksum
  
  Comment Example is taken from WikiPedia.
  Thanks to WikiPedia for the detailed explanation.
  '''
  def __checksum(self, msg):
    # Example IP header
    # 4500 0073 0000 4000 4011
    # b861 c0a8 0001 c0a8 00c7
    data_hex = unpack("!HHHHHHHHHH", msg[:20])
    
    # Add up all the numbers
    # 4500 + 0073 + 0000 + 4000 + 
    # 4011 + c0a8 + 0001 + c0a8 + 00c7 = 2479C
    raw_sum = sum(data_hex)

    # Get the highest byte
    # 2 479C => 2
    high_part = (raw_sum & 0xffff0000) >> 16

    # Get the rest bytes
    # 2 479C => 479C
    low_part = raw_sum & 0xffff

    # Add them up and flip
    # B681 is the final result for this example
    # 2 + 479C => 479E
    # (~479E)  => B681
    # 0100 0111 1001 1110 (479E)
    # 1011 1000 0110 0001 (B861)
    final_sum = (~(high_part + low_part) & 0xffff)

    return final_sum
