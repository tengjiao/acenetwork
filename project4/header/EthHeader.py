from _struct import pack, unpack

# Ethernet Header Reference
# NOTE: 
#   Attributes are not aligned with within the graph 

#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |             ~    Destination MAC Address(6s)    ~             |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                 ~  Source MAC Address(6s)   ~                 |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                        Protocol Type(H)                       |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

class EthHeader:

  '''
  Constructor
  '''
  def __init__(self, sMAC, dMAC):
    self.dest_MAC = dMAC
    self.src_MAC = sMAC
    self.p_type = 0x0800   # Protocol Type (IPv4 = 0x0800)
  
  '''
  Pack the packet into struct of OS c code
  '''
  def pack(self):
    eth_header = pack('!6s6sH' ,\
                      self.dest_MAC, self.src_MAC,\
                      self.p_type)
    
    return eth_header
  
  
  '''
  Unpack the packet into object of this python code
  
  Return the data following ARP header
  '''
  def unpack(self, raw_data):
    [self.dest_MAC, self.src_MAC, self.p_type] = unpack("!6s6sH", raw_data[:14])
    
    data = raw_data[14:]
    return data
