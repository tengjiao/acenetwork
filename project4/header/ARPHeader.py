import socket
from _struct import pack, unpack

# ARP Header Reference
# NOTE: 
#   HA length: Hardware address length
#   PA length: Protocol address length
#   4 Addresses are not aligned with within the graph 

#     0               1               2               3   
#     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |          Hardware Type        |          Protocol Type        |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |    HA length  |   PA length   |             Opcode            |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                 ~  Source MAC Address(6s)   ~                 |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                 ~   Source IP Address(4s)   ~                 |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |             ~    Destination MAC Address(6s)    ~             |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |             ~    Destination IP Address(4s)   ~               |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

class ARPHeader:
  '''
  Constructor
  '''
  def __init__(self, sMAC, sIP, dMAC, dIP):
    self.h_type = 1        # Hardware Type (Ethernet = 1)
    self.p_type = 0x0800   # Protocol Type (IPv4 = 0x0800)
    self.h_len = 6         # Hardware Address Length (MAC: 6 Bytes)
    self.p_len = 4         # Protocol Address Length (IP: 4 Bytes)
    self.opcode = 0        # Operation Code (1 for request; 2 for reply)
    self.src_MAC = sMAC
    self.src_IP = sIP      # Source IP Address
    self.dest_MAC = dMAC
    self.dest_IP = dIP     # Destination IP Address

  '''
  Pack the packet into struct of OS c code
  '''
  def pack(self):
    self.opcode = 1   # Operation Code (1 for request)
    
    sIP =  socket.inet_aton(self.src_IP)
    dIP = socket.inet_aton(self.dest_IP)
    
    # B: 1B, H:2B, 4s:4B
    # HH   -> 4B
    # HH   -> 4B
    # 6s4s -> 10B 
    # 6s4s -> 10B
    arp_header = pack('!HHBBH6s4s6s4s' ,\
                      self.h_type, self.p_type,\
                      self.h_len, self.p_len, self.opcode,\
                      self.src_MAC, sIP,\
                      self.dest_MAC, dIP)
    
    return arp_header
  
  
  '''
  Unpack the packet into object of this python code
  
  Return the data following ARP header
  '''
  def unpack(self, raw_data):
    [self.h_type, self.p_type] = unpack("!HH", raw_data[:4])
    [self.h_len, self.p_len, self.opcode] = unpack("!ssH", raw_data[4:8])
    [self.src_MAC, sIP] = unpack("!6s4s", raw_data[8:18])
    [self.dest_MAC, dIP] = unpack("!6s4s", raw_data[18:28])
    
    self.src_IP = socket.inet_ntoa(sIP)
    self.dest_IP = socket.inet_ntoa(dIP)

    data = raw_data[28:]
    
    return data
