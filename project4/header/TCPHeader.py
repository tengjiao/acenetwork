import socket
from _struct import pack, unpack
from random import randint


# TCP Header Reference (Thanks to Silver Moon)

#     0               1               2               3   
#     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |          Source Port          |       Destination Port        |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                        Sequence Number                        |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                    Acknowledgment Number                      |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |  Data |           |U|A|P|R|S|F|                               |
#    | Offset| Reserved  |R|C|S|S|Y|I|            Window             |
#    |       |           |G|K|H|T|N|N|                               |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |           Checksum            |         Urgent Pointer        |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                    Options                    |    Padding    |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#    |                             data                              |
#    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

class TCPHeader:
  '''
  Constructor
  '''
  def __init__(self, srcPort, destPort):
    # tcp header fields
    # 1st line
    self.source = srcPort   # source port
    self.dest = destPort   # destination port
    
    # 2nd, 3rd line
    self.seq = randint(0, (1 << 16)-1)
    self.ack = 0
    
    # 4th line
    self.doff = 5      # 4 bit field, size of tcp header, 5 * 4 = 20 bytes
    self.flag_urg = 0  # indicates that the Urgent pointer field is significant
    self.flag_ack = 0  # indicates that the Acknowledgment field is significant
    self.flag_psh = 0  # Push function
    self.flag_rst = 0  # Reset the connection
    self.flag_syn = 1  # Synchronize sequence numbers
    self.flag_fin = 0  # No more data from sender
    self.window = 8192 # maximum allowed window size
    
    # 5th line
    self.check = 0
    self.urg_ptr = 0

     
  '''
  Pack the packet into struct of OS c code
  '''
  def pack(self):
    
    offset = self.doff << 12
    res = 0
    flags = (self.flag_urg << 5) + (self.flag_ack << 4) +\
            (self.flag_psh << 3) + (self.flag_rst << 2) +\
            (self.flag_syn << 1) + self.flag_fin
            
    offset_res_flags = offset + res + flags
    
    # B: 1B, H:2B, 4s:4B
    # HH  -> 4B
    # LL  -> 4B, 4B
    # HH -> 4B 
    # HH  -> 4B
    tcp_header = pack('!HHLLHHHH' ,\
                  self.source, self.dest,\
                  self.seq, self.ack,\
                  offset_res_flags,  self.window,\
                  self.check, self.urg_ptr)
        
    return tcp_header
  
  '''
  Unpack the packet into object of this python code
  
  Return the data following tcp header
  '''
  def unpack(self, raw_data):
    [self.source, self.dest] = unpack("!HH", raw_data[:4])
    [self.seq, self.ack] = unpack("!LL", raw_data[4: 12])
    [offset_res_flags,  self.window] = unpack("!HH", raw_data[12: 16])
    [self.check, self.urg_ptr] = unpack("!HH", raw_data[16: 20])

    self.doff = offset_res_flags >> 12
    flags = offset_res_flags & int('111111',2)
    self.flag_urg = (flags >> 5) & 0x01
    self.flag_ack = (flags >> 4) & 0x01
    self.flag_psh = (flags >> 3) & 0x01
    self.flag_rst = (flags >> 2) & 0x01
    self.flag_syn = (flags >> 1) & 0x01
    self.flag_fin = flags & 0x01

    data = raw_data[20:]

    return data
  
  
  
