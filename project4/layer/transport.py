import sys
import subprocess
import time
# import the parent directory
sys.path.append('..')
from layer.network import *
from header.TCPHeader import *  

TIME_OUT = 60
CWND_UNIT = 1024
INIT_ADV_WND = 1024

'''
This is the transport (TCP) layer
'''
class Transport:

  '''
  Constructor
  '''
  def __init__(self, destIP, destPort):
    self.srcIP = self.__getIPAddr()
    self.srcPort = self.__getFreePort()
    self.destIP = destIP
    self.destPort = destPort
    self.network = Network(self.srcIP, destIP)
    self.s_tcp_header = None
  
  '''
  Three way hand shaking
  '''
  def connect(self):
    # first way shaking
    s_tcp_header = TCPHeader(self.srcPort, self.destPort)
    s_tcp_header.flag_syn = 1

    self.__sendTcpSeg(s_tcp_header,"")
    
    # second way shaking
    r_tcp_header, r_data = self.__recvTcpSeg()
    
    # if not successful, try again
    while r_tcp_header.flag_syn != 1 or\
          r_tcp_header.flag_ack != 1 or\
          r_tcp_header.ack != s_tcp_header.seq + 1 :
      self.__sendTcpSeg(s_tcp_header,"")
      r_tcp_header, r_data = self.__recvTcpSeg()

    # record the initial advertise window
    INIT_ADV_WND = r_tcp_header.window

    # third way shaking
    s_tcp_header.seq = r_tcp_header.ack
    s_tcp_header.ack = r_tcp_header.seq + 1
    s_tcp_header.flag_ack = 1
    s_tcp_header.flag_syn = 0

    self.__sendTcpSeg(s_tcp_header,"")

    self.s_tcp_header = s_tcp_header

  '''
  Send data to the server
  '''
  def sendall(self, data):

    cwnd = CWND_UNIT
    adv_wnd = INIT_ADV_WND

    self.s_tcp_header.flag_fin = 0
    self.s_tcp_header.flag_ack = 1
    self.s_tcp_header.flag_syn = 0
    
    while data != "":

      wnd = min(adv_wnd, cwnd)

      data_sent = data[:wnd]

      self.__sendTcpSeg(self.s_tcp_header, data_sent)
      r_tcp_header, r_data = self.__recvTcpSeg()

      # adjust the congestion window
      if r_tcp_header == False:
        cwnd = CWND_UNIT
      else:
        cwnd += cwnd

      # If the cwnd is larger than it
      # should be, resize to 1000.
      if cwnd > CWND_UNIT * 1000 :
        cwnd = CWND_UNIT * 1000

      # resend lost segments
      while r_tcp_header and\
            r_tcp_header.ack != self.s_tcp_header.seq +len(data_sent):
        self.__sendTcpSeg(self.s_tcp_header, data_sent)
        r_tcp_header, r_data = self.__recvTcpSeg()

      # send the rest data
      data = data[len(data_sent):]

      # reset the advertise window
      adv_wnd = r_tcp_header.window

      # set the new sequence
      self.s_tcp_header.seq = r_tcp_header.ack

  '''
  Receive data from the server
  '''
  def recv(self):
    data_buffer = {}
    has_bar = False

    # for progress bar
    data_received = 0    
    content_length = 0

    while True:
      r_tcp_header, r_data = self.__recvTcpSeg()

      if r_data == False:
        self.__sendTcpSeg(self.s_tcp_header,"")
        continue

      # FOR TEST
      data_buffer_l = sorted(data_buffer.items())
      if len(data_buffer_l) > 2 and\
         self.s_tcp_header.ack != r_tcp_header.seq:
        self.__sendTcpSeg(self.s_tcp_header,"")
        continue

      data_buffer[r_tcp_header.seq] = r_data

      # Get the content length for progress bar
      if len(data_buffer) == 1 and not has_bar:
        pos = r_data.find("Content-Length:")
        if pos > 0:
          content_length = int(r_data[(pos+len("Content-Length:")):].split()[0])
          content_length += r_data.find("/r/n/r/n")
          has_bar = True

      # Draw progress bar
      if has_bar:
        data_received += len(r_data)
        percent = round(data_received*100.0/content_length, 2)

        if percent >= 100:
          percent = 100

        progess = int( round(percent / 3.3))

        sys.stdout.write('\r')
        sys.stdout.write("[%-30s] \t%.2f%%" % ('='*progess, percent))
        sys.stdout.flush()

      if r_tcp_header.flag_fin == 1 and\
         data_buffer_l[-1][0] == r_tcp_header.seq and\
         len(data_buffer_l[-1][1]) == len(r_data):

        self.s_tcp_header.flag_fin = 0
        self.s_tcp_header.flag_ack = 1

        self.s_tcp_header.flag_syn = 0
        self.s_tcp_header.ack = r_tcp_header.seq + 1
        self.__sendTcpSeg(self.s_tcp_header,"")
        break

      # ACK the received data
      # For duplicate packets, I will ACK one more
      # time in case the previous ACK is lost.
      self.s_tcp_header.seq = r_tcp_header.ack
      self.s_tcp_header.ack = r_tcp_header.seq + len(r_data)
      self.s_tcp_header.flag_ack = 1
      self.s_tcp_header.flag_syn = 0
      self.__sendTcpSeg(self.s_tcp_header,"")

    # return all the data
    data_buffer = sorted(data_buffer.items())
    all_data = ""

    # draw the line feed after the progress bar
    if has_bar:
      sys.stdout.write('\n') 

    for data in data_buffer:
      all_data += data[1]
    
    return all_data

  '''
  Tear down the connection
  '''  
  def close(self):
    self.s_tcp_header.flag_fin = 1
    self.s_tcp_header.flag_ack = 0
    self.s_tcp_header.flag_syn = 0

    self.__sendTcpSeg(self.s_tcp_header,"")

  '''
  Send a TCP segment (tcp_header + data)
  '''    
  def __sendTcpSeg(self, tcp_header, data):
    
    tcp_header.check = 0
    psh = self.__getPseudoHeader(tcp_header.pack(), self.srcIP, self.destIP, data)
    checksum_header = psh + tcp_header.pack() + data
 
    tcp_header.check = self.__checksum(checksum_header)

    TCP_segment = tcp_header.pack() + data
    
    return self.network.send(TCP_segment)

  '''
  Receive a TCP segment (tcp_header + data)
  
  If time out, return a False
  '''    
  def __recvTcpSeg(self):
    
    tcp_header = TCPHeader(self.srcPort, self.destPort)
    tcp_header_4check = TCPHeader(self.srcPort, self.destPort)

    stampTime = time.time()

    while True:
      currentTime = time.time()
      
      # If time out, return False
      if currentTime - stampTime > TIME_OUT:
        return False, False
      
      raw_data = self.network.recv()

      # If raw_data == None, return false

      if raw_data == None:
        return False, False

      data = tcp_header.unpack(raw_data)
      
      tcp_header_4check.unpack(raw_data)
      tcp_header_4check.check = 0

      psh = self.__getPseudoHeader(tcp_header_4check.pack(), self.destIP, self.srcIP, data)
      checksum_header = psh + tcp_header_4check.pack() + data

      _check = self.__checksum(checksum_header)
      

      if _check != tcp_header.check and False:
        continue

      if tcp_header.source == self.destPort and \
          tcp_header.dest == self.srcPort:
        return tcp_header, data

  '''
  Get self IP address
  '''
  def __getIPAddr(self):
    strs = subprocess.check_output('ifconfig').split()
    ip_find = ""
    for substr in strs:
      substr = substr.strip()
      if "addr:" in substr:
        offset = len("addr:")
        ip_find = substr[offset:].strip()
        return ip_find

  '''
  Get free port
  '''
  def __getFreePort(self):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(("", 0))
    sock.listen(1)
    ipaddr, port = sock.getsockname()
    sock.close
    return port  

  '''
  Calculate the checksum
  '''  
  def __checksum(self, msg):  
    
    if len(msg)%2 ==1:
      msg = msg + '\0'
    
    data_hex = unpack("!"+"H"*(len(msg)/2), msg[:len(msg)])
    raw_sum = sum(data_hex)

    high_part = (raw_sum & 0xffff0000) >> 16

    low_part = raw_sum & 0xffff

    final_sum = (~(high_part + low_part) & 0xffff)

    return final_sum

  '''
  Calculate the psuedo header for a given tcp_header
  '''   
  def __getPseudoHeader(self, tcp_header, srcIP, destIP, data):
    # pseudo header fields
    source_address = socket.inet_aton( srcIP )
    dest_address = socket.inet_aton( destIP )
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header) + len(data)

    psh = pack('!4s4sBBH' , source_address , dest_address ,\
                            placeholder , protocol , tcp_length)
    return psh

# FOR TEST
if __name__ == '__main__':
  transport = Transport("129.10.113.143", 80)
  print "*************** source IP and port *****************"
  print transport.srcIP
  print transport.srcPort
  #transport.recv()
  transport.connect()
  print "connect successful"

  print transport.sendall("GET {url} HTTP/1.1 \r\n"
                          "Host: {host} \r\n"
                          "Connection: keep-alive \r\n"
                          "\r\n\r\n".format(url="/", host="cs5700sp15.ccs.neu.edu"))
  all_data = transport.recv()
  print all_data

