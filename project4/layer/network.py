import socket
import time
import sys
# import the parent directory
sys.path.append('..')
from header.IPHeader import IPHeader
from layer.datalink import *

'''
This is the network (IP) layer
'''

class Network:
  '''
  Constructor
  '''
  def __init__(self, sourceIP, descIP):
    self.saddr = sourceIP
    self.daddr = descIP
    self.datalink = DataLink()
  
  '''
  Send the packet to the destination
  '''
  def send(self, TCP_segment):
    
    ipheader = IPHeader(self.saddr,self.daddr)
    ipheader.tot_len = 20 + len(TCP_segment)
    IP_packet = ipheader.pack() + TCP_segment

    #Send the packet finally - the port specified has no effect
    return self.datalink.send(IP_packet)

  '''
  Receive the packet from the source
  
  If the source IP and desc IP matches
  then return the data
  '''
  def recv(self):
    
    # This ip header is used to receive data
    # What ip addr is passed into object does not matter
    ipheader = IPHeader(self.saddr, self.daddr)
    
    while True:

      raw_data = self.datalink.recv()
      
      if raw_data == None:
        continue

      # This method will overwrite the ip header
      # and return the following data
      data = ipheader.unpack(raw_data)
 
      if ipheader.saddr == self.daddr and \
          ipheader.daddr == self.saddr:
        return data
  
  '''
  Close the socket connection
  '''
  def close(self):
    self.sock_send.close()
    self.sock_recv.close()

# FOR TEST
if __name__ == '__main__':
  network = Network("192.168.88.5","129.10.113.143")
  #print "**********receive data**********"
  network.recv()
