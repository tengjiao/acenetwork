import sys
import subprocess
import socket
import binascii
# import the parent directory
sys.path.append('..')
from header.EthHeader import EthHeader
from header.ARPHeader import ARPHeader

class DataLink:
  
  '''
  Constructor
  '''
  def __init__(self):
    self.sIP, self.sMAC = self.__IPAndMAC()
    self.dIP = self.__gatewayIP()
    self.dMAC = self.__gatewayMAC()

    #create raw sockets
    try:
      sock_send = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
      sock_recv = socket.socket(socket.AF_PACKET,\
                                socket.SOCK_RAW,\
                                socket.ntohs(0x0800))
    except socket.error , msg:
      print 'Socket could not be created. Error Code : ' +\
             str(msg[0]) + ' Message ' + msg[1]
      sys.exit()

    self.sock_send = sock_send
    self.sock_recv = sock_recv

  '''
  Send data to the server
  '''
  def send(self, IP_packet):
    ethHeader = EthHeader(self.sMAC, self.dMAC)
    ethFrame = ethHeader.pack() + IP_packet
    
    return self.sock_send.sendto(ethFrame, ("eth0", 0))

  '''
  Receive data from the server
  '''
  def recv(self):
    ethHeader = EthHeader(self.sMAC, self.dMAC)

    while True:
      raw_data = self.sock_recv.recvfrom(32768)[0]
      #print raw_data
      data = ethHeader.unpack(raw_data)

      if ethHeader.dest_MAC == self.sMAC\
         and ethHeader.src_MAC == self.dMAC:
        return data

  '''
  Close the connection to the server
  '''    
  def close(self):
    self.sock_send.close()
    self.sock_send.close()

  '''
  Get the MAC address of the gateway router
  '''
  def __gatewayMAC(self):
    arp_s_sock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
    arp_r_sock = socket.socket(socket.AF_PACKET,\
                               socket.SOCK_RAW,\
                               socket.htons(0x0806)) # 0x0806 (used by ARP)
    arp_r_sock.settimeout(2)

    destARP_MAC = "00:00:00:00:00:00"
    destARP_MAC = destARP_MAC.replace(':','').decode('hex')
    arpHeader = ARPHeader(self.sMAC, self.sIP, destARP_MAC, self.dIP)

    broadcastMAC = "FF:FF:FF:FF:FF:FF"
    broadcastMAC = broadcastMAC.replace(':','').decode('hex')
    ethHeader = EthHeader(self.sMAC, broadcastMAC)
    ethHeader.p_type = 0x0806

    ethFrame = ethHeader.pack() + arpHeader.pack()
    
    arp_s_sock.sendto(ethFrame, ("eth0", 0))
    
    while True:
      raw_data = arp_r_sock.recvfrom(4096)[0]

      data = ethHeader.unpack(raw_data)

      if ethHeader.dest_MAC == self.sMAC\
         and ethHeader.p_type == 0x0806:
        arpHeader.unpack(data)
        if arpHeader.src_IP == self.dIP\
           and arpHeader.dest_IP == self.sIP:
          break;

    arp_s_sock.close()
    arp_r_sock.close()
    
    return ethHeader.src_MAC

  '''
  Get the gateway IP for finding MAC in ARP 
  '''
  def __gatewayIP(self):
    route_table = subprocess.check_output(['route', '-n'])

    '''
    Here we want to get the gateway ip of the following string

    Kernel IP routing table
    Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
    0.0.0.0         192.168.88.2    0.0.0.0         UG    0      0        0 eth0
    '''
    second_line =  route_table.strip().split('\n')[1].strip()
    third_line =  route_table.strip().split('\n')[2].strip()

    gateway_index = second_line.split().index('Gateway')
    gateway_ip = third_line.split()[gateway_index]

    return gateway_ip

  '''
  Get the source IP and MAC for finding MAC in ARP 
  '''  
  def __IPAndMAC(self):
    ifconfig = subprocess.check_output('ifconfig')

    ip_find = ''
    MAC_find = ''

    strs = ifconfig.split()
    for substr in strs:
      substr = substr.strip()
      if "addr:" in substr:
        offset = len("addr:")
        ip_find = substr[offset:].strip()
        break

    strs = ifconfig.split('  ')
    for substr in strs:
      substr = substr.strip()
      if "HWaddr " in substr:
        offset = len("HWaddr ")
        MAC_find = substr[offset:].strip()
        break
    
    #print MAC_find
    MAC_find = MAC_find.replace(':','').decode('hex')
    return ip_find, MAC_find

# FOR TEST
if __name__ == '__main__':
  datalink = DataLink()
  datalink.recv()
