import re
import urllib
import urlparse
import socket
import sys
# import the parent directory
sys.path.append('..')
from layer.transport import *

HTTP_PORT = 80
  
'''
This is the application (http) layer
'''
class Http:

  '''
  Constructor
  '''
  def __init__(self, url):
    self.url = url

  '''
  return the crawled page in the given url  
  '''  
  def get(self):
    url_parser = urlparse.urlparse(self.url)
    dest_ip = socket.gethostbyname(url_parser.netloc)
    dest_port = HTTP_PORT
    transport = Transport(dest_ip, dest_port)
    transport.connect()
    
    path = ""
    if url_parser.path == "":
      path = "/"
    else:
      path = url_parser.path
    
    transport.sendall("GET {path} HTTP/1.0\r\n"
                      "Host: {host}\r\n"
                      "Connection: keep-alive"
                      "\r\n\r\n".format(path=path, host=url_parser.netloc))
    
    all_data = transport.recv()
    transport.close()
    
    return all_data
    
  '''
  save_file : data -> [save to a file]
  
  save the crawled web page into current directory  
  '''
  def save_file(self, data):
    url_path = urlparse.urlparse(self.url).path
    
    save_path = ""
    
    if url_path == "" or url_path[-1] == "/":
      save_path = "index.html"
    else:
      save_path = url_path.split("/")[-1]
      pass

    # parse the header
    # If not 200, then exit
    datas = data.split()

    if len(datas) < 2:
      print "NO DATA RECEIVED"
      sys.exit()      
    
    if datas[1] != '200':
      print "NONE-200 RESPONSE STATUS"
      sys.exit()

    # get rid of the header
    header_offset = data.find("\r\n\r\n")
    header_offset += len("\r\n\r\n")

    print data[:header_offset]
    print "File Downloaded '" + save_path +"'"

    data = data[header_offset:]
  
    f = open(save_path, "w+")
    f.write(data)
    f.close()

# FOR TEST
if __name__ == '__main__':
    http1 = Http("http://cs5700sp15.ccs.neu.edu")
    data = http1.get()
    http1.save_file(data)
