import Queue

MAX_SPACE = 10 * 1024 * 1024

'''
'''
class CacheController():
  
  def __init__(self):
    self.used_space = 0
    # key: path
    # value: cache
    self.cache_hash = {}
    self.cache_heap = Queue.PriorityQueue()
  
  def find_cache(self, path):
    #print path

    if path in self.cache_hash:
      cache = self.cache_hash[path]
      return cache.content
    else:
      return False

  def update_cache(self, path):
    cache = self.cache_hash[path]
    cache.frequency = cache.frequency + 1
    print "cache hit: get the cache data"
    print cache.frequency
    print cache.path

    _cache = self.cache_heap.get()

    print _cache.frequency
    print _cache.path

    self.cache_heap.put(_cache)
    print "cache hit: return cache data"

  
  def add_cache(self, path, content):
    # cache evict
    while len(content) + self.used_space > MAX_SPACE :
      evict_cache = self.cache_heap.get()
      self.used_space -= len(evict_cache.content)
    
    cache = Cache(path, content)
    print "save data cache"
    self.cache_hash[path] = cache
    self.used_space += len(content)
    self.cache_heap.put(cache)
'''
'''  
class Cache():
  
  def __init__(self, path, content):
    self.frequency = 0
    self.path = path
    self.content = content
  
  def __cmp__(self, other):
    return cmp(self.frequency, other.frequency)
