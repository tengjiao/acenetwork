import random
from _struct import pack, unpack
import re

REQUEST = 0
RESPONSE = 1

# DNS Packet ( Thanks to this great work of Prof. Mislove )

#     +---------------------+
#     |        Header       |
#     +---------------------+
#     |       Question      | one question for the name server
#     +---------------------+
#     |        Answer       | one answer to the question
#     +---------------------+
#     |      Authority      | Not used in this project
#     +---------------------+
#     |      Additional     | Not used in this project
#     +---------------------+

# DNS Header

#      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                     ID                        |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |QR|   Opcode  |AA|TC|RD|RA|    Z   |   RCODE   |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                    QDCOUNT                    |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                    ANCOUNT                    |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                    NSCOUNT                    |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                    ARCOUNT                    |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

# DNS Question

#      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     /           ~         QNAME         ~           /
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                     QTYPE                     |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                     QCLASS                    |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

# DNS Answer

#      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     /           ~         ANAME         ~           /
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                     ATYPE                     |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                     ACLASS                    |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                      TTL                      |
#     |                                               |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |                    RDLENGTH                   |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
#     /           ~         RDATA         ~           /
#     /           ~                       ~           /
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

class DNSPacket(object):
  '''
  Constructor
  '''
  def __init__(self):
    ##############################################
    #               DNS HEADER                   #
    ##############################################
    self.ID = random.randint(0, 65535)

    # 0 for your requests, 1 for the responses you receive
    self.QR = REQUEST

    # Opcode: 0 standard query
    self.Opcode = 0

    # 0 means non-authoritative answer
    self.AA = 0

    # 1 truncated, 0 non-truncated
    # truncated (1) message should be dropped right away
    self.TC = 0

    # Recursion Desired
    # 1 means that you desire recursion query
    self.RD = 1

    # Recursion Available
    # 1 means available
    self.RA = 1

    # Reserved bit, usually set to 0
    self.Z = 0

    # Response code
    #   - 0 No error condition
    #   - 1 Format error
    #   - 2 Server failure
    #   - 3 Name Error
    #   - 4 Not Implemented
    #   - 5 Refused
    self.RCODE = 0

    # Number of questions and answers
    # Both are set to 1
    self.QDCOUNT = 1
    self.ANCOUNT = 1

    # Name Server count and A Record count
    # Both are set to 0 and ignored
    self.NSCOUNT = 0
    self.ARCOUNT = 0

    ##############################################
    #               DNS QUESTION                 #
    ##############################################
    self.QNAME = 0
    self.QTYPE = 0
    self.QCLASS = 0

    ##############################################
    #               DNS ANSWER                   #
    ##############################################
    self.ANAME = 0
    self.ATYPE = 0
    self.ACLASS = 0
    self.TTL = 0
    self.RDLENGTH = 0
    self.RDATA = 0

  '''
  Pack DNS packet
  Here, we will only send answers.
  '''
  def pack(self):
    # wrap up all attributes

    print "QR: " + str(self.QR)
    print "Opcode: " + str(self.Opcode)
    print "AA: " + str(self.AA)
    print "TC: " + str(self.TC)
    print "RD: " + str(self.RD)
    print "RA: " + str(self.RA)
    print "Z: " + str(self.Z)
    print "RCODE: " + str(self.RCODE)

    attrs = self.QR << 15
    print "attrs 1: " + str(attrs)
    attrs += self.Opcode << 11
    print "attrs 2: " + str(attrs)
    attrs += self.AA << 10
    print "attrs 3: " + str(attrs)
    attrs += self.TC << 9
    print "attrs 4: " + str(attrs)
    attrs += self.RD << 8
    print "attrs 5: " + str(attrs)
    attrs += self.RA << 7
    print "attrs 6: " + str(attrs)
    attrs += self.Z << 4
    print "attrs 7: " + str(attrs)
    attrs += self.RCODE
    print "attrs 8: " + str(attrs)

    print "ID: " + str(self.ID)
    print "attrs: " + str(attrs)
    print "QDCOUNT: " + str(self.QDCOUNT)
    print "ANCOUNT: " + str(self.ANCOUNT)
    print "NSCOUNT: " + str(self.NSCOUNT)
    print "ARCOUNT: " + str(self.ARCOUNT)

    dns_header = pack('!HHHHHH' ,\
                      self.ID, attrs,\
                      self.QDCOUNT, self.ANCOUNT,\
                      self.NSCOUNT, self.ARCOUNT)

    dns_q = pack('!' + str(len(self.QNAME)) + 'sHH',\
                       self.QNAME, self.QTYPE, self.QCLASS)

    print "ANAME: " + str(self.ANAME)
    print "ATYPE: " + str(self.ATYPE)
    print "ACLASS: " + str(self.ACLASS)
    print "TTL: " + str(self.TTL)
    print "RDLENGTH: " + str(self.RDLENGTH)
    print "RDATA: " + self.RDATA
    #print "len(ANAME): " + str(len(self.ANAME))

    dns_a = pack('!HHHIH4s',\
                 self.ANAME, self.ATYPE, self.ACLASS,\
                 self.TTL, self.RDLENGTH, self.RDATA)

    return dns_header + dns_q + dns_a

  '''
  Unpack DNS packet
  Here, we wil only receive questions.
  '''
  def unpack(self, data):
    [self.ID, attrs] = unpack("!HH", data[0:4])
    [self.QDCOUNT, self.ANCOUNT] = unpack("!HH", data[4:8])
    [self.NSCOUNT, self.ARCOUNT] = unpack("!HH", data[8:12])

#      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
#     |QR|   Opcode  |AA|TC|RD|RA|    Z   |   RCODE   |
#     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

    # Assign all the attributes
    self.QR = attrs >> 15
    self.Opcode = (attrs >> 11) & 0x000F
    self.AA = (attrs >> 10) & 0x0001
    self.TC = (attrs >> 9) & 0x0001
    self.RD = (attrs >> 8) & 0x0001
    self.RA = (attrs >> 7) & 0x0001
    self.Z = (attrs >> 4) & 0x0007
    self.RCODE = attrs & 0x000F

    # Here the unpack one should be the receiving ones
    # The last empty byte is removed
    str_len = len(data) - 4 - 12
    [self.QNAME] = unpack("!"+str(str_len)+"s", data[12: len(data)-4])

    print self.QNAME

    [self.QTYPE, self.QCLASS] = unpack("!HH", data[len(data)-4 :])



