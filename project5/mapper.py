import socket
from multiprocessing.pool import ThreadPool, Process
import time
import threading
import logging
import math
import subprocess
import json

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )

mapping = {}
mapping_lock = threading.Lock()

ec2_IPs = ['52.0.73.113',
           '52.16.219.28',
           '52.11.8.29',
           '52.8.12.101',
           '52.28.48.84',
           '52.68.12.77',
           '52.74.143.5',
           '52.64.63.125',
           '54.94.214.108']

ec2_IP_location = {'52.0.73.113':(39.0437,-77.4875),
                   '52.16.219.28':(53.3331,-6.2489),
                   '52.11.8.29':(45.8399,-119.7006),
                   '52.8.12.101':(37.3394,-121.8950),
                   '52.28.48.84':(50.1167,8.6833),
                   '52.68.12.77':(35.6850,139.7514),
                   '52.74.143.5':(1.2931,103.8558),
                   '52.64.63.125':(-33.8615,151.2055),
                   '54.94.214.108':(-23.4733,-46.6658)}

SERVER_PORT = 55555
MAX_TIMEOUT = 1000000
MAX_DIST = 40000 # 2 * pi * earth_r

'''
client_IP -> Server_IP

Find best server IP for a certain client.
'''
def best_server_IP(client_IP):
  if client_IP not in mapping:
    DB_IP = best_IP_from_DB(client_IP)
    __find_best_server_IP(client_IP)
    return DB_IP
  else:
    __find_best_server_IP(client_IP)
    return mapping[client_IP]

'''
client_IP -> Server_IP

Find best server IP from local DB or web service.
This function gives the GEOGRAPHICALLY best server IP.
'''
def best_IP_from_DB(client_IP):
  output = subprocess.check_output("curl ipinfo.io/"+client_IP ,shell=True)
  location = json.loads(output)['loc']
  lan = float(location.split(',')[0])
  lon = float(location.split(',')[1])
  
  best_IP = ec2_IPs[0]
  min_dist = MAX_DIST
    
  for IP, location in ec2_IP_location.iteritems():
    lan_s = location[0]
    lon_s = location[1]
    dist = __distance(lan, lon, lan_s, lon_s)
    
    if dist < min_dist:
      min_dist = dist
      best_IP = IP
  
  return best_IP

'''
Fire a new thread to run the find_best_server_IP function
'''
def __find_best_server_IP(client_IP):
#   t = threading.Thread(target=find_best_server_IP,\
#                          args=(client_IP,))
#   t.setDaemon(True) # don't hang on exit
#   t.start()
  process = Process(target=find_best_server_IP, args=(client_IP,))
  process.start()
  print "Running find function"

'''
client_IP -> best server IP to serve the client
'''
def find_best_server_IP(client_IP):
  print "Enter find function"
  pool = ThreadPool(processes=9)
  async_result = [None] * 9
  return_val = [None] * 9

  logger = logging.getLogger('find_best_server_IP')

  for i in range(0,9):
    async_result[i] = pool.apply_async( __server_performance,\
                                        (ec2_IPs[i], client_IP))
  time.sleep(2)

  for i in range(0,9):
    if async_result[i].ready():
      return_val[i] = async_result[i].get()
    else:
      return_val[i] = MAX_TIMEOUT

  logger.debug(ec2_IPs)
  logger.debug(return_val)
  logger.debug("client IP " + client_IP)
  logger.debug("best server IP "+min(return_val)[1])

  # key - value
  mapping_lock.acquire()
  mapping[client_IP] =  min(return_val)[1]
  mapping_lock.release()

'''
server_IP, client_IP -> the ping time between them (in ms)
'''
def __server_performance(server_IP, client_IP):
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((server_IP, SERVER_PORT))
    s.send("Client_IP: "+client_IP)
    ping_time = s.recv(4096)
    s.close()
    return float(ping_time), server_IP
  except:
    print server_IP + " cannot be connected."
    return MAX_TIMEOUT, server_IP
  
'''
set the global variable: server port
'''
def set_server_port(server_PORT):
  global SERVER_PORT
  SERVER_PORT = server_PORT

'''

Location1, Location2 -> Distance between them

Calculate the geo distance by two geo location
according to Haversine formula
'''  
def __distance(la1, lo1, la2, lo2):
  radius = 6371 # earth radius in km

  delta_la = math.radians(la2-la1)
  delta_lo = math.radians(lo2-lo1)
  
  haversin_la = math.sin(delta_la/2) ** 2
  haversin_lo = math.sin(delta_lo/2) ** 2
  
  haversin_d_r = haversin_la + haversin_lo * math.cos(math.radians(la1)) \
                                           * math.cos(math.radians(la2))
  distance = 2 * radius * math.asin(math.sqrt(haversin_d_r))

  return distance


# For test
if __name__ == '__main__':
#   best_IP_from_DB("8.8.8.8")
#   print __distance(38.898556,-77.037852,38.897147,-77.043934)
#   print best_IP_from_DB(42.3424,-71.0878)
  pass
